# description:

this is 2 work in progress workarounds for issues like https://github.com/ValveSoftware/steam-for-linux/issues/4982

* `preload_based` is the (currently **failing**) attempt to wrap syscalls using LD_PRELOAD to trick steam into thinking the drive is smaller
  see https://github.com/ValveSoftware/steam-for-linux/issues/3226#issuecomment-413288346 for an actually working preload based workaround
* `fuse_based` is the (currently **unknown** if working, but also never implemented completely, so likely not, since the issue doesn't occur on current kernel and steam versions for me anymore) attempt to use a FUSE fs to do the same
